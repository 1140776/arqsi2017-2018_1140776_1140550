using Microsoft.VisualStudio.TestTools.UnitTesting;
using MedicamentoWebApi.Controllers;
using Microsoft.EntityFrameworkCore;
using MedicamentoWebApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace UnitTestProject1
{
    [TestClass]
    public class MedicamentoWebAPITest
    {
        [TestMethod]
        public void GetMedicamentoNotNullTest()
        {
            Medicamento medicamento = new Medicamento { Id = 1, Nome = "Brufen" };

           string nome =  medicamento.Nome;
            var result = "Brufen";
            Assert.AreEqual(nome,result);
        }

        [TestMethod]
        public void GetFarmacoNotNullTest()
        {
            Farmaco farmaco = new Farmaco { Id = 1, Nome = "Ibuprofeno" };

            string nome = farmaco.Nome;
            var result = "Ibuprofeno";
            Assert.AreEqual(nome, result);
        }

        [TestMethod]
        public void GetPosologiaNotNullTest()
        {
            Posologia posologia = new Posologia { Id = 1, Descricao = "diario", ApresentacaoId = 1 };

            string nome = posologia.Descricao;
            var result = "diario";
            Assert.AreEqual(nome, result);
        }

        [TestMethod]
        public void GetApresentacaoNotNullTest()
        {
            Apresentacao apresentacao = new Apresentacao { Id = 1, Forma = "Xarope", Concentracao = 1, Quantidade = 40  };

            string nome = apresentacao.Forma;
            var result = "Xarope";
            Assert.AreEqual(nome, result);
            double conc = apresentacao.Concentracao;
            var result1 = 1;
            Assert.AreEqual(conc, result1);
            double quant = apresentacao.Quantidade;
            var result2 = 40;
            Assert.AreEqual(quant, result2);
        }
        [TestMethod]
        public void GetAccountRegisterLoginNotNullTest()
        {
            AccountRegisterLogin account = new AccountRegisterLogin { Email = "a@a.pt", Password="Qwerty1!" };

            string email = account.Email;
            var result = "a@a.pt";
            Assert.AreEqual(email, result);
            string pass = account.Password;
            var result1 = "Qwerty1!";
            Assert.AreEqual(pass, result1);
        }
        
    }
}
