// ./routes/receitaRoutes.js
var express    = require('express');        // call express
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var User =  require('../models/user');
var VerifyToken = require('../verifyToken');
var Receita = require('../models/receita');
var ReceitaDTO = require('../DTOs/receitaDTO');
var PrescricaoDTO = require('../DTOs/prescricaoDTO');
var router = express.Router();
const request = require('request');
var userid = 0;
function hasRole(userEmail, role, func){
    User.findOne({
        email: userEmail.user
    }, function(err, user){
        if(err) throw err;

        if(!user){
            res.json({
                success: false, message:'Authentication failed.'
            });
        }else if(user){
            //check if role matches
            userid = user._id;
            if(role === 'medico'){
                func(role === 'medico' && user.medico === true && user.farmaceutico === false);
            }else if(role === 'farmaceutico'){
                func(role === 'farmaceutico' && user.farmaceutico === true && user.medico === false);
            }
        }
    })
}
var options = { 
    method:'POST',
    url: 'http://localhost:63656/api/Account/Token',
    body: {"email":"a@a.pt","password":"Qwerty1!"},
    headers:{'Content-Type': 'application/json'},
    json: true 
}
createToken = function (cb) {
    request(options, function (error, response, body) {
        if (error) {
            console.log("Erro: " + error);
            token = null;
            cb(token);
        } else {
            if (body !== null  && body !== undefined ) {
                token = "Bearer " + body.token;
                cb(token);
            }
        } 
    });
}
// on routes that end in /Receita
// ----------------------------------------------------
router.route('/')
// create a receita (accessed at POST http://localhost:8080/api/receita)
.post(VerifyToken, function(req, res) {
        //check if user's role matches 'medico'
    hasRole(req.user, 'medico' , function(decision){
        if(!decision){
            return res.status(401).send(
                {auth: false, token: null, message: 'You have no authorization!'});
        }else{
            var receita = new Receita();      // create a new instance of the Receita model
            receita.data = req.body.data; 
            receita.utente = req.body.utente;
            receita.medico = userid;
            var bodyprescricoes = req.body.prescricoes;
            receita.prescricoes = new Array(bodyprescricoes.length).fill({});
            // using the ES6 let syntax, it creates a new binding
            // every single time the function is called
            // read more here: http://exploringjs.com/es6/ch_variables.html#sec_let-const-loop-heads
            createToken(function (token) {
                if(!token){
                    return res.status(500).send(
                        {auth: false, token: null, message: 'Internal server error!'});
                    }
                for (let i = 0; i < bodyprescricoes.length; i++) { 
                //var auth = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJiMDc4ZjViOS1iN2M0LTQ4ZmYtYWY5ZS00YzQzYWRlNmI2YTgiLCJzdWIiOiJhQGEucHQiLCJleHAiOjE1MDk5Nzc2OTcsImlzcyI6Imh0dHA6Ly9zZW1lbnRld2ViYXBpLmxvY2FsIiwiYXVkIjoiaHR0cDovL3NlbWVudGV3ZWJhcGkubG9jYWwifQ.9FCdvKbNsibQfuDvtAEmpxACs0n5iFA_KdKOgDy3JLg'; 
                //{'Host': 'www.example.com', 'Authorization': auth}
                    var urlString = 'http://localhost:63656/api/Apresentacao/'+ bodyprescricoes[i].apresentacao_id +'/Receita';
                    request({method:'GET', url: urlString, headers:{authorization : token}, json: true }, (err, resp, body) => {
                        if (err || body == undefined) {res.status(401).send(err); return console.log(err); }
                            receita.prescricoes[i]._id = i+1;
                            receita.prescricoes[i].apresentacao_id = bodyprescricoes[i].apresentacao_id;
                            receita.prescricoes[i].apresentacao = body.forma +" "+ body.concentracao;
                            receita.prescricoes[i].farmaco = body.farmacoNome;
                            receita.prescricoes[i].posologia_id  = bodyprescricoes[i].posologia_id;
                            receita.prescricoes[i].posologiaPrescrita  = bodyprescricoes[i].posologiaPrescrita;
                            receita.prescricoes[i].quantidadePrescrita = bodyprescricoes[i].quantidadePrescrita;
                            if(bodyprescricoes.length == i+1 ){
                            // save the Receita and check for errors 
                                receita.save(function(err) {
                                if (err)
                                    res.send(err);

                                res.json({ message: 'Receita created!' });
                            });
                        }
                    });
                }
            });
        }
    });
})
// get all the Receita (accessed at GET http://localhost:8080/api/Receita)
    .get(VerifyToken,function(req, res) {
        //check if user's role matches 'medico'
        hasRole(req.user, 'medico' , function(decision){
            if(!decision){
                 //check if user's role matches 'farmaceutico'
                hasRole(req.user, 'farmaceutico' , function(decision){
                    if(decision){
                    return res.status(401).send(
                        {auth: false, token: null, message: 'You have no authorization!'});
                    }else{
                        //its a utente
                        Receita.find({utente: userid},function(err, receita) {
                            if (err)
                                res.send(err);
        
                            res.json(receita);
                        });
                    }
                });
           /* return res.status(401).send(
                {auth: false, token: null, message: 'You have no authorization!'});*/
            }else{
                Receita.find({medico: userid},function(err, receita) {
                    if (err)
                        res.send(err);

                    res.json(receita);
                });
            }
        });
    });

// on routes that end in /Receita/:receita_id
// ----------------------------------------------------
router.route('/:receita_id')

// get the receita with that id (accessed at GET http://localhost:8080/api/Receita/:receita_id)
    .get(VerifyToken,function(req, res) {
        
        hasRole(req.user, 'farmaceutico' , function(decision){
            if(!decision){
                hasRole(req.user, 'medico' , function(decision){
                    if(!decision){
                     //its a utente
                        Receita.findOne({_id : req.params.receita_id, utente: userid},function(err, receita) {
                            if (err)
                                res.send(err);
    
                            if (receita !== null && receita !== undefined){
                                var receitaDTO = new ReceitaDTO();
                                receitaDTO.id = receita.id;
                                receitaDTO.data = receita.data;
                                receitaDTO.utente = receita.utente;
                                receitaDTO.medico = receita.medico;
                                for (var i=0;i<receita.prescricoes.length;i++){
                                    receitaDTO.prescricoes.push(receita.prescricoes[i]._id);
                                }
                                res.json(receitaDTO);
                            }
                            res.json(receita);
                        });
                    }else{
                        //its a medico
                        Receita.findOne({ _id : req.params.receita_id, medico: userid},function(err, receita) {
                            if (err)
                                res.send(err);
        
                            if (receita !== null && receita !== undefined){
                                var receitaDTO = new ReceitaDTO();
                                receitaDTO.id = receita.id;
                                receitaDTO.data = receita.data;
                                receitaDTO.utente = receita.utente;
                                receitaDTO.medico = receita.medico;
                                for (var i=0;i<receita.prescricoes.length;i++){
                                    receitaDTO.prescricoes.push(receita.prescricoes[i]._id);
                                }
                                res.json(receitaDTO);
                            }
                            res.json(receita);  
                        });
                    }
                });
            }else{
                Receita.findById(req.params.receita_id, function(err, receita) {
                    if (err)
                        res.send(err);
                
                    if (receita !== null && receita !== undefined){
                        var receitaDTO = new ReceitaDTO();
                        receitaDTO.id = receita.id;
                        receitaDTO.data = receita.data;
                        receitaDTO.utente = receita.utente;
                        receitaDTO.medico = receita.medico;
                        for (var i=0;i<receita.prescricoes.length;i++){
                            receitaDTO.prescricoes.push(receita.prescricoes[i]._id);
                        }
                        res.json(receitaDTO);
                    }
                    res.json(receita);   
                });
            }
        });
    })
// update the Receita with this id (accessed at PUT http://localhost:8080/api/Receita/:Receita_id)
    .put(VerifyToken,function(req, res) {       
        //check if user's role matches 'medico'
        hasRole(req.user, 'medico' , function(decision){
            if(!decision){
                return res.status(401).send(
                    {auth: false, token: null, message: 'You have no authorization!'});
            }else{
            // use our Receita model to find the Receita we want
                Receita.findOne({_id : req.params.receita_id, utente: userid}, function(err, receita) {       
                    if (err)
                        res.send(err);
                    var prescricoesList =  receita.prescricoes;
                    for(var i = 0; i < prescricoesList.length; i++){
                        if(prescricoesList[i].aviamentos.length !== 0){
                            return res.status(401).send(
                                {auth: false, token: null, message: 'You cant change this receita!'});
                        }
                    }
                    var newData = req.body.data;
                    var newUtente = req.body.utente;
                    if(newData || newUtente){
                        receita.data = newData;
                        receita.utente = newUtente;
                        // save the receita
                        receita.save(function(err) {
                            if (err)
                                res.send(err);
        
                            res.json({ message: 'Receita updated!' });
                        });
                    }else{
                        res.status(304).send(
                            {auth: false, token: null, message: 'Not Modified!'});
                    }
                });
            }
        });
    });
    /*// delete the receita with this id (accessed at DELETE http://localhost:8080/api/receitas/:receita_id)
    .delete(function(req, res) {
        Receita.remove({
            _id: req.params.receita_id
        }, function(err, receita) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });*/
// ----------------------------------------------------
router.route('/:receita_id/Prescricao/:prescricao_id')

// get the receita with that id (accessed at GET http://localhost:8080/api/Receita/:receita_id/Prescricao/:prescricao_id)
    .get(VerifyToken,function(req, res) {
        var prescricaoDTO = new PrescricaoDTO();
        hasRole(req.user, 'farmaceutico' , function(decision){
            if(!decision){
                hasRole(req.user, 'medico' , function(decision){
                    if(!decision){
                    //its a utente
                        Receita.findOne({_id : req.params.receita_id, utente: userid}, function(err, receita) {
                            if (err)
                                res.send(err);
                        
                            if (receita !== null && receita !== undefined){
                                prescricaoDTO.id = receita.prescricoes[req.params.prescricao_id-1]._id;
                                prescricaoDTO.apresentacao_id = receita.prescricoes[req.params.prescricao_id-1].apresentacao_id;
                                prescricaoDTO.posologiaPrescrita = receita.prescricoes[req.params.prescricao_id-1].posologiaPrescrita;
                                res.json(prescricaoDTO);
                            }
                            res.json(receita);           
                        });
                    }else{
                     //its a medico
                        Receita.findOne({_id : req.params.receita_id, medico: userid}, function(err, receita) {
                            if (err)
                                res.send(err);
                        
                            if (receita !== null && receita !== undefined){
                                prescricaoDTO.id = receita.prescricoes[req.params.prescricao_id-1]._id;
                                prescricaoDTO.apresentacao_id = receita.prescricoes[req.params.prescricao_id-1].apresentacao_id;
                                prescricaoDTO.posologiaPrescrita = receita.prescricoes[req.params.prescricao_id-1].posologiaPrescrita;
                                res.json(prescricaoDTO);
                            }
                            res.json(receita);           
                        });
                    }
                });
            }else{
                //its a farmaceutico
                Receita.findById(req.params.receita_id, function(err, receita) {
                    if (err)
                        res.send(err);
                    
                    if (receita !== null && receita !== undefined){
                        prescricaoDTO.id = receita.prescricoes[req.params.prescricao_id-1]._id;
                        prescricaoDTO.apresentacao_id = receita.prescricoes[req.params.prescricao_id-1].apresentacao_id;
                        prescricaoDTO.posologiaPrescrita = receita.prescricoes[req.params.prescricao_id-1].posologiaPrescrita;
                        res.json(prescricaoDTO);
                    }
                    res.json(receita);           
                });
            }
        });
    })
// update the Receita with this id (accessed at PUT http://localhost:8080/api/Receita/:Receita_id)
    .put(VerifyToken,function(req, res) {       
        //check if user's role matches 'medico'
        hasRole(req.user, 'medico' , function(decision){
            if(!decision){
                return res.status(401).send(
                    {auth: false, token: null, message: 'You have no authorization!'});
            }else{
            // use our Receita model to find the Receita we want
                Receita.findOne({_id : req.params.receita_id, utente: userid}, function(err, receita) {       
                    if (err)
                        res.send(err);
                    var prescricoesList =  receita.prescricoes;
                    for(var i = 0; i < prescricoesList.length; i++){
                        if(prescricoesList[i]._id ==req.param.prescricao_id && prescricoesList[i].aviamentos.length !== 0){
                            var newapresentacao_id = req.body.apresentacao_id;
                            var newapresentacao = req.body.apresentacao;
                            var newfarmaco = req.body.farmaco;
                            var newposologiaPrescrita = req.body.posologiaPrescrita;
                            if(newapresentacao_id || newapresentacao || newfarmaco|| newposologiaPrescrita){
                                // save the receita
                                receita.data = newapresentacao_id;
                                receita.utente = newapresentacao;
                                receita.farmaco = newfarmaco;
                                receita.posologiaPrescrita = newposologiaPrescrita;
                                receita.save(function(err) {
                                    if (err)
                                        res.send(err);
                
                                    res.json({ message: 'Receita updated!' });
                                });
                            }
                        }
                    }
                    return res.status(401).send(
                        {auth: false, token: null, message: 'You have no authorization!'});
                });
            }
        });
    });
    // ----------------------------------------------------
router.route('/:receita_id/Prescricao/:prescricao_id/aviar')
// update the Prescricao with this id (accessed at PUT http://localhost:8080/api/Receita/:Receita_id/Prescricao/:prescricao_id/aviar)
    .put(VerifyToken,function(req, res) {
        //its a farmaceutico
        hasRole(req.user, 'farmaceutico' , function(decision){
            if(!decision){
                return res.status(401).send(
                    {auth: false, token: null, message: 'You have no authorization!'});
            }else{
                Receita.findById(req.params.receita_id, function(err, receita) {
                    if (err)
                        res.send(err);
            
                    var aviamentos = receita.prescricoes[req.params.prescricao_id-1].aviamentos;
                    var totalAviado = 0;
                    //valida quantidade disponivel
                    for(var i = 0; i < aviamentos.length; i ++){
                        totalAviado += aviamentos[i].quantidadeAviada;
                    }
                    if(totalAviado+req.body.quantidadeAviada > receita.prescricoes[req.params.prescricao_id-1].quantidadePrescrita){
                        res.json({ message: 'A quantidade excede o prescrito!' });
                    }else{
                        receita.prescricoes[req.params.prescricao_id-1].aviamentos.push({"farmaceutico" : req.body.farmaceutico,"quantidadeAviada": req.body.quantidadeAviada, "dataAviamento": req.body.dataAviamento });
                    }
                    receita.save(function(err) {
                        if (err)
                            res.send(err);
        
                        res.json({ message: 'Prescricao updated!' });
                    });
                });
            }
        });
    });
module.exports = router;
