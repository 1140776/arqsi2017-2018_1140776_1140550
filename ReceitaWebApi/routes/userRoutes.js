// ./routes/userRoutes.js

var express    = require('express'); // call express
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../config');
var User =  require('../models/user');
var VerifyToken = require('../verifyToken');
var router = express.Router(); 

function hasRole(userEmail, role, func){
    User.findOne({
        email: userEmail.user
    }, function(err, user){
        if(err) throw err;

        if(!user){
            res.json({
                success: false, message:'Authentication failed.'
            });
        }else if(user){
            //check if role matches
            func(role === 'medico' && user.medico === true);
        }
    })
}
//get all the users
router.get('/', VerifyToken, function(req, res, next) {
    //check if user's role matches 'medico'
    hasRole(req.user, 'medico' , function(decision){
        if(!decision){
            return res.status(403).send(
                {auth: false, token: null, message: 'You have no authorization!'});
        }else{
            User.find(function (err, users){
                if(err)
                    res.send(err);
                
                    res.json(users);
            })
        }
    });
});
//register a user
router.post('/register', function(req,res){
    var user = new User();
    user.name = req.body.name;
    hashedPassword = bcrypt.hashSync(req.body.password,8);
    user.password = hashedPassword;
    user.email = req.body.email;
    user.medico = req.body.medico;
    user.farmaceutico = req.body.farmaceutico;

    //save the user and check errors
    user.save(function(err){
        console.log("in save");
        if(err)
            return res.status(500).send("There was a problem registering the user.");

        res.json({message : 'user registered!'});
    })
});
//get the user's token
router.post('/login', function(req,res){
    //find the user
    User.findOne({
        email: req.body.email
    }, function(err, user){
        if(err) throw err;

        if(!user){
            res.json({ sucess: false, message : 'Authentication failed.'});          
        }else if(user){
            var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
            if (!passwordIsValid){
                return res.status(401).send(
                    {auth : false, token: null, message : 'Authentication failed.'});
            }else{
                const payload = {
                    user : user.email
                };
                var token = jwt.sign(payload, config.secret, {
                    expiresIn : 86400 // expires in 24 hours
                });
                //return the information including token as JSON
                res.json({
                    success : true,
                    message : 'Enjoy your token!',
                    token: token
                });
            }
        }
    });
});
router.get('/logout', function(req, res) {
    res.status(200).send({ auth: false, token: null });
  });
module.exports = router;
