// ./routes/utenteRoutes.js
var express    = require('express'); // call express
var jwt = require('jsonwebtoken');
var User =  require('../models/user');
var PrescricaoDTO = require('../DTOs/prescricaoDTO');
var Receita = require('../models/receita');
var VerifyToken = require('../verifyToken');
var router = express.Router(); 

function hasRole(userEmail, role, func){
    User.findOne({
        email: userEmail.user
    }, function(err, user){
        if(err) throw err;

        if(!user){
            res.json({
                success: false, message:'Authentication failed.'
            });
        }else if(user){
            //check if role matches
            func(role === 'medico' && user.medico === true);
        }
    })
}
//get all the users
router.get('/:utente_id/prescricao/poraviar/:data?', VerifyToken, function(req, res, next) {
    //check if user's role matches 'medico'
    hasRole(req.user, 'medico' , function(decision){
        if(!decision){
            return res.status(403).send(
                {auth: false, token: null, message: 'You have no authorization!'});
        }else{
            var prescricoesporaviar = [];
            Receita.find({utente : req.params.utente_id}, function(err, receita){
                if (err)
                    res.send(err);
                
                for(var i = 0 ; i< receita.length; i++){
                    for(var j = 0 ; j< receita[i].prescricoes.length; j++){
                        if(receita[i].prescricoes[j].aviamentos.length == 0){
                            prescricoesporaviar.push(receita[i].prescricoes[j]);
                        }else if( req.params.data !== undefined){
                            for(var k = 0 ; k< receita[i].prescricoes[j].aviamentos.length; k++){
                                var dataAviamento = new Date(receita[i].prescricoes[j].aviamentos[k].dataAviamento);
                                var compareDate = new Date(req.params.data);
                                if(dataAviamento > compareDate){
                                    prescricoesporaviar.push(receita[i].prescricoes[j]);
                                }
                            }
                        }
                    }
                }
                res.json(prescricoesporaviar);
            });
        }
    });
});
module.exports = router;