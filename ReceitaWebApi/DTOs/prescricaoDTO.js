// ./models/prescricaoDTO.js
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');
var PrescricaoDTOSchema   = new Schema({
    id: Number,
    apresentacao_id: Number,
    posologiaPrescrita : String
});
PrescricaoDTOSchema.plugin(autoIncrement.plugin, 'PrescricaoDTO');
module.exports = mongoose.model('PrescricaoDTO', PrescricaoDTOSchema);