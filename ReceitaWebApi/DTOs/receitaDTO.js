// app/models/receitaDTO.js
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');
var ReceitaDTOSchema   = new Schema({
    id: Number,
    data: Date,
    utente: Number,
    medico: Number,
    prescricoes: Array
});
ReceitaDTOSchema.plugin(autoIncrement.plugin, 'ReceitaDTO');
module.exports = mongoose.model('ReceitaDTO', ReceitaDTOSchema);