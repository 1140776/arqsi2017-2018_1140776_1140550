// app/models/receita.js
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');

var ReceitaSchema   = new Schema({
    data: Date,
    utente: Number,
    medico: Number,
    prescricoes:[{
        _id : Number,
        apresentacao_id: Number,
        apresentacao : String,
        farmaco : String,
        posologia_id : Number,
        posologiaPrescrita : String,
        quantidadePrescrita : Number,
        aviamentos : [{
            farmaceutico : Number,
            quantidadeAviada : Number,
            dataAviamento : Date
        }]
    }]
});
ReceitaSchema.plugin(autoIncrement.plugin, 'Receita');
module.exports = mongoose.model('Receita', ReceitaSchema);
