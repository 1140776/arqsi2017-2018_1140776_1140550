//./models/user.js

var mongoose =  require('mongoose');
var Schema =  mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');

//set up a mongoose model and pass it using module.exports
var UserSchema  = new Schema({
    name : String,
    password : String,
    email : String,
    medico : Boolean,
    farmaceutico : Boolean
});
UserSchema.plugin(autoIncrement.plugin, 'User');
module.exports = mongoose.model('User', UserSchema);
