﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace MedicamentoWebApi.Models
{
    public class SeedData
    {
        public static void Initialize(MedicamentoWebApiContext context)
        {
            context.Database.EnsureCreated();

            // Look for any Medicamentos.
            if (context.Medicamento.Any())
            {
                return;   // DB has been seeded
            }

            var farmacos = new Farmaco[] {
                     new Farmaco{
                         Nome = "Ibuprofeno"
                     },

                     new Farmaco
                     {
                         Nome = "Paracetamol "
                     }
            };
            foreach (Farmaco f in farmacos)
            {
                context.Farmaco.Add(f);
            }
            context.SaveChanges();

            var medicamentos = new Medicamento[] {
                     new Medicamento
                     {
                         Nome = "Brufen"
                     },

                     new Medicamento
                     {
                         Nome = "Ben-u-ron"
                     }
            };
            foreach (Medicamento m in medicamentos)
            {
                context.Medicamento.Add(m);
            }
            context.SaveChanges();

            Apresentacao[] apresentacoes = new Apresentacao[] {
                     new Apresentacao
                     {
                         Forma = "Comprimido",
                         Concentracao = 600.0,
                         Quantidade = 24.0,
                         FarmacoID = 1,
                         MedicamentoID = 1
                     },

                     new Apresentacao
                     {
                         Forma = "Comprimido",
                         Concentracao = 400.0,
                         Quantidade = 24.0,
                         FarmacoID = 1,
                         MedicamentoID = 1
                     },
                     new Apresentacao
                     {
                         Forma = "Xarope ",
                         Concentracao = 100.0,
                         Quantidade = 75.0,
                         FarmacoID = 2,
                         MedicamentoID = 2
                     }
            };
            foreach (Apresentacao a in apresentacoes)
            {
                context.Apresentacao.Add(a);
            }

            context.SaveChanges();

            var posologias = new Posologia[] {
                     new Posologia
                     {
                         Descricao = "1 x 8h-8h",
                         ApresentacaoId = 1
                     },

                     new Posologia
                     {
                         Descricao = "1 x 12h-12h",
                         ApresentacaoId = 1
                     }
            };
            foreach (Posologia p in posologias)
            {
                context.Posologia.Add(p);
            }
            context.SaveChanges();
        }
    }
}
