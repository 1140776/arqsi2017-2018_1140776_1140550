﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicamentoWebApi.Models
{
    public class Posologia
    {
        public int Id { get; set; }

        public string Descricao { get; set; }

        public int ApresentacaoId { get; set; }

        public Apresentacao Apresentacao { get; set; }

    }
}
