﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MedicamentoWebApi.Models
{
    public class Apresentacao
    {
        public int Id { get; set; }

        public string Forma { get; set; }

        public double Concentracao { get; set; }
        
        public double Quantidade { get; set; }

        public int FarmacoID { get; set; }
        public Farmaco Farmaco { get; set; }

        public int MedicamentoID { get; set; }
        public Medicamento Medicamento { get; set; }
    }
}
