﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicamentoWebApi.Models
{
    public class Farmaco
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
