﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MedicamentoWebApi.Migrations
{
    public partial class M3_listaDeApresentacoes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Medicamento_Apresentacao_ApresentacaoId",
                table: "Medicamento");

            migrationBuilder.DropIndex(
                name: "IX_Medicamento_ApresentacaoId",
                table: "Medicamento");

            migrationBuilder.DropColumn(
                name: "ApresentacaoId",
                table: "Medicamento");

            migrationBuilder.AddColumn<int>(
                name: "MedicamentoId",
                table: "Apresentacao",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Apresentacao_MedicamentoId",
                table: "Apresentacao",
                column: "MedicamentoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Apresentacao_Medicamento_MedicamentoId",
                table: "Apresentacao",
                column: "MedicamentoId",
                principalTable: "Medicamento",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Apresentacao_Medicamento_MedicamentoId",
                table: "Apresentacao");

            migrationBuilder.DropIndex(
                name: "IX_Apresentacao_MedicamentoId",
                table: "Apresentacao");

            migrationBuilder.DropColumn(
                name: "MedicamentoId",
                table: "Apresentacao");

            migrationBuilder.AddColumn<int>(
                name: "ApresentacaoId",
                table: "Medicamento",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Medicamento_ApresentacaoId",
                table: "Medicamento",
                column: "ApresentacaoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Medicamento_Apresentacao_ApresentacaoId",
                table: "Medicamento",
                column: "ApresentacaoId",
                principalTable: "Apresentacao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
