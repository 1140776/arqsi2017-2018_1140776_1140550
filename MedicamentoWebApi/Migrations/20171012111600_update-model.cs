﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MedicamentoWebApi.Migrations
{
    public partial class updatemodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Apresentacao_Medicamento_MedicamentoId",
                table: "Apresentacao");

            migrationBuilder.RenameColumn(
                name: "MedicamentoId",
                table: "Apresentacao",
                newName: "MedicamentoID");

            migrationBuilder.RenameIndex(
                name: "IX_Apresentacao_MedicamentoId",
                table: "Apresentacao",
                newName: "IX_Apresentacao_MedicamentoID");

            migrationBuilder.AlterColumn<int>(
                name: "MedicamentoID",
                table: "Apresentacao",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Apresentacao_Medicamento_MedicamentoID",
                table: "Apresentacao",
                column: "MedicamentoID",
                principalTable: "Medicamento",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Apresentacao_Medicamento_MedicamentoID",
                table: "Apresentacao");

            migrationBuilder.RenameColumn(
                name: "MedicamentoID",
                table: "Apresentacao",
                newName: "MedicamentoId");

            migrationBuilder.RenameIndex(
                name: "IX_Apresentacao_MedicamentoID",
                table: "Apresentacao",
                newName: "IX_Apresentacao_MedicamentoId");

            migrationBuilder.AlterColumn<int>(
                name: "MedicamentoId",
                table: "Apresentacao",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Apresentacao_Medicamento_MedicamentoId",
                table: "Apresentacao",
                column: "MedicamentoId",
                principalTable: "Medicamento",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
