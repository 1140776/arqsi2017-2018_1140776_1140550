﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MedicamentoWebApi.Models;
using Microsoft.AspNetCore.Authorization;
using MedicamentoWebApi.DTOs;

namespace MedicamentoWebApi.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Apresentacao")]
    public class ApresentacoesController : Controller
    {
        private readonly MedicamentoWebApiContext _context;

        public ApresentacoesController(MedicamentoWebApiContext context)
        {
            _context = context;
        }

        // GET: api/Apresentacao
        [HttpGet]
        public IQueryable<ApresentacaoDTO> GetApresentacao()
        {
            var apresentacao = _context.Apresentacao.Select(a =>
               new ApresentacaoDTO
               {
                   Id = a.Id,
                   Forma = a.Forma,
                   Concentracao = a.Concentracao,
                   Quantidade = a.Quantidade
               });
            return apresentacao;
        }

        // GET: api/Apresentacao/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetApresentacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacao = await _context.Apresentacao.Select(a =>
               new ApresentacaoDTO {
                   Id = a.Id,
                   Forma = a.Forma,
                   Concentracao = a.Concentracao,
                   Quantidade = a.Quantidade
            }).SingleOrDefaultAsync(a => a.Id == id);

            if (apresentacao == null)
            {
                return NotFound();
            }

            return Ok(apresentacao);
        }
        // GET: api/Apresentacao/5/Receita
        [HttpGet("{id}/Receita")]
        public async Task<IActionResult> GetApresentacaoParaReceita([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacao = await _context.Apresentacao.Include(f => f.Farmaco).Select(a =>
               new ApresentacaoParaReceitaDTO
               {
                   Id = a.Id,
                   Forma = a.Forma,
                   Concentracao = a.Concentracao,
                   Quantidade = a.Quantidade,
                   FarmacoNome = a.Farmaco.Nome,
               }).SingleOrDefaultAsync(a => a.Id == id);

            if (apresentacao == null)
            {
                return NotFound();
            }

            return Ok(apresentacao);
        }
        // GET: api/Apresentacao/5/Posologia
        [HttpGet("{id}/Posologia")]
        public async Task<IActionResult> GetPosologiaApresentacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<Posologia> listaPosologias = await _context.Posologia.ToListAsync();
            var posologia = listaPosologias.Where(p => p.ApresentacaoId == id).Select(P => P.Descricao).ToList();

            if (posologia == null)
            {
                return NotFound();
            }

            return Ok(posologia);
        }
        // PUT: api/Apresentacao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutApresentacao([FromRoute] int id, [FromBody] Apresentacao apresentacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != apresentacao.Id)
            {
                return BadRequest();
            }

            _context.Entry(apresentacao).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApresentacaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Apresentacao
        [HttpPost]
        public async Task<IActionResult> PostApresentacao([FromBody] Apresentacao apresentacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Apresentacao.Add(apresentacao);
            await _context.SaveChangesAsync();
            //DTO

            var dto = new ApresentacaoDTO
            {
                Id = apresentacao.Id,
                Forma = apresentacao.Forma,
                Concentracao = apresentacao.Concentracao,
                Quantidade = apresentacao.Quantidade
            };


            return CreatedAtAction("GetApresentacao", new { id = apresentacao.Id }, dto);
        }

        // DELETE: api/Apresentacao/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteApresentacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacao = await _context.Apresentacao.SingleOrDefaultAsync(m => m.Id == id);
            if (apresentacao == null)
            {
                return NotFound();
            }

            _context.Apresentacao.Remove(apresentacao);
            await _context.SaveChangesAsync();

            return Ok(apresentacao);
        }

        private bool ApresentacaoExists(int id)
        {
            return _context.Apresentacao.Any(e => e.Id == id);
        }
    }
}