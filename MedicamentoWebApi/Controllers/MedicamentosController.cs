﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MedicamentoWebApi.Models;
using Microsoft.AspNetCore.Authorization;
using MedicamentoWebApi.DTOs;

namespace MedicamentoWebApi.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Medicamento")]
    public class MedicamentosController : Controller
    {
        private readonly MedicamentoWebApiContext _context;

        public MedicamentosController(MedicamentoWebApiContext context)
        {
            _context = context;
        }

        // GET: api/Medicamento
        /*[HttpGet]
        public IEnumerable<Medicamento> GetMedicamento()
        {
            return _context.Medicamento;
        }*/

        // GET: api/Medicamento/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicamento = await _context.Medicamento.Select(m =>
            new MedicamentoDTO
            {
                Id = m.Id,
                Nome = m.Nome
            }).SingleOrDefaultAsync(m => m.Id == id);

            if (medicamento == null)
            {
                return NotFound();
            }

            return Ok(medicamento);
        }

        // GET: api/Medicamento/?nome=brufen
        [HttpGet]
        public IActionResult GetMedicamento(string nome)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var lista = _context.Medicamento.Select(m =>
            new MedicamentoDTO
            {
                Id = m.Id,
                Nome = m.Nome
            });

            if (nome == null)
            {
                return Ok(lista);
            }

            var medicamento = lista.SingleOrDefault(m => m.Nome == nome);

            if (medicamento == null)
            {
                return NotFound();
            }

            return Ok(medicamento);
        }

        // GET: api/Medicamento/5/Apresentacoes
        [HttpGet("{id}/Apresentacoes")]
        public async Task<IActionResult> GetApresentacoesMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            List<Apresentacao> listaApresentacoes = await _context.Apresentacao.ToListAsync();
            List<int> apresentacoes = listaApresentacoes.Where(m => m.MedicamentoID == id).Select(a => a.Id).ToList();

            if (apresentacoes == null)
            {
                return NotFound();
            }

            return Ok(apresentacoes);
        }

        // GET: api/Medicamento/5/Posologias
        [HttpGet("{id}/Posologias")]
        public async Task<IActionResult> GetPosologiasMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            List<Posologia> listaPosologias = await _context.Posologia.Include(p=>p.Apresentacao).ToListAsync();
            List<int> posologias = listaPosologias.Where(p => p.Apresentacao.MedicamentoID == id).Select(a => a.Id).ToList();

            if (posologias == null)
            {
                return NotFound();
            }

            return Ok(posologias);
        }

        // PUT: api/Medicamentos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedicamento([FromRoute] int id, [FromBody] Medicamento medicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != medicamento.Id)
            {
                return BadRequest();
            }

            _context.Entry(medicamento).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Medicamento
        [HttpPost]
        public async Task<IActionResult> PostMedicamento([FromBody] Medicamento medicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Medicamento.Add(medicamento);
            await _context.SaveChangesAsync();
            //DTO
            var dto = new MedicamentoDTO
            {
                Id = medicamento.Id,
                Nome = medicamento.Nome
            };


            return CreatedAtAction("GetMedicamento", new { id = medicamento.Id }, dto);
        }

        // DELETE: api/Medicamentos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicamento = await _context.Medicamento.SingleOrDefaultAsync(m => m.Id == id);
            if (medicamento == null)
            {
                return NotFound();
            }

            _context.Medicamento.Remove(medicamento);
            await _context.SaveChangesAsync();

            return Ok(medicamento);
        }

        private bool MedicamentoExists(int id)
        {
            return _context.Medicamento.Any(e => e.Id == id);
        }
    }
}