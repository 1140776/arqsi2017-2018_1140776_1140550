﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MedicamentoWebApi.Models;
using Microsoft.AspNetCore.Authorization;
using MedicamentoWebApi.DTOs;

namespace MedicamentoWebApi.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Farmaco")]
    public class FarmacosController : Controller
    {
        private readonly MedicamentoWebApiContext _context;

        public FarmacosController(MedicamentoWebApiContext context)
        {
            _context = context;
        }

        // GET: api/Farmaco
      /*  [HttpGet]
        public IEnumerable<Farmaco> GetFarmaco()
        {
            return _context.Farmaco;
        }*/

        // GET: api/Farmaco/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.Select(f => 
            new FarmacoDTO {
                Id = f.Id,
                Nome = f.Nome
            }).SingleOrDefaultAsync(m => m.Id == id);

            if (farmaco == null)
            {
                return NotFound();
            }

            return Ok(farmaco);
        }

        // GET: api/Farmaco/?nome=ibuprofeno
        [HttpGet]
        public IActionResult GetFarmaco(string nome)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var lista = _context.Farmaco.Select(f =>
           new FarmacoDTO
           {
               Id = f.Id,
               Nome = f.Nome
           });

            if (nome == null)
            {
                return Ok(lista);
            }

            var farmaco = lista.SingleOrDefault(f => f.Nome == nome);


            if (farmaco == null)
            {
                return NotFound();
            }

            return Ok(farmaco);
        }

        // GET: api/Farmaco/5/Apresentacoes
        [HttpGet("{id}/Apresentacoes")]
        public async Task<IActionResult> GetApresentacoesFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            List<Apresentacao> listaApresentacoes = await _context.Apresentacao.ToListAsync();
            List<int> apresentacoes = listaApresentacoes.Where(m => m.FarmacoID== id).Select(a => a.Id).ToList();

            if (apresentacoes == null)
            {
                return NotFound();
            }

            return Ok(apresentacoes);
        }

        // GET: api/Farmaco/5/Posologias
        [HttpGet("{id}/Posologias")]
        public async Task<IActionResult> GetPosologiasFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            List<Posologia> listaPosologias = await _context.Posologia.Include(p => p.Apresentacao).ToListAsync();
            List<int> posologias = listaPosologias.Where(p => p.Apresentacao.FarmacoID == id).Select(a => a.Id).ToList();

            if (posologias == null)
            {
                return NotFound();
            }

            return Ok(posologias);
        }

        // GET: api/Farmaco/5/Medicamentos
        [HttpGet("{id}/Medicamentos")]
        public async Task<IActionResult> GetMedicamentosFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            List<Apresentacao> listaApresentacoes = await _context.Apresentacao.Include(a => a.Medicamento).ToListAsync();
            List<int> medicamentos = listaApresentacoes.Where(p => p.FarmacoID == id).Select(a => a.MedicamentoID).Distinct().ToList();

            if (medicamentos == null)
            {
                return NotFound();
            }

            return Ok(medicamentos);
        }

        // PUT: api/Farmaco/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFarmaco([FromRoute] int id, [FromBody] Farmaco farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != farmaco.Id)
            {
                return BadRequest();
            }

            _context.Entry(farmaco).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FarmacoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Farmaco
        [HttpPost]
        public async Task<IActionResult> PostFarmaco([FromBody] Farmaco farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Farmaco.Add(farmaco);
            await _context.SaveChangesAsync();
            //DTO

            var dto = new FarmacoDTO
            {
                Id = farmaco.Id,
                Nome = farmaco.Nome
            };

            return CreatedAtAction("GetFarmaco", new { id = farmaco.Id }, dto);
        }

        // DELETE: api/Farmaco/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(m => m.Id == id);
            if (farmaco == null)
            {
                return NotFound();
            }

            _context.Farmaco.Remove(farmaco);
            await _context.SaveChangesAsync();

            return Ok(farmaco);
        }

        private bool FarmacoExists(int id)
        {
            return _context.Farmaco.Any(e => e.Id == id);
        }
    }
}