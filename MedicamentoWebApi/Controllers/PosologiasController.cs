﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MedicamentoWebApi.Models;
using Microsoft.AspNetCore.Authorization;
using MedicamentoWebApi.DTOs;

namespace MedicamentoWebApi.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Posologias")]
    public class PosologiasController : Controller
    {
        private readonly MedicamentoWebApiContext _context;

        public PosologiasController(MedicamentoWebApiContext context)
        {
            _context = context;
        }

        // GET: api/Posologias
        [HttpGet]
        public IQueryable<PosologiaDTO> GetPosologia()
        {
            var posologia = _context.Posologia.Select(p =>
              new PosologiaDTO
              {
                  Id = p.Id,
                  Descricao = p.Descricao
              });
            return posologia;
        }

        // GET: api/Posologias/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPosologia([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var posologia = await _context.Posologia.Include(a => a.Apresentacao).Select(p => 
                new PosologiaDTO
                {
                    Id = p.Id,
                    Descricao = p.Descricao
                }).SingleOrDefaultAsync(p => p.Id == id);
            
            if (posologia == null)
            {
                return NotFound();
            }

            return Ok(posologia);
        }

        // PUT: api/Posologias/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPosologia([FromRoute] int id, [FromBody] Posologia posologia)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != posologia.Id)
            {
                return BadRequest();
            }

            _context.Entry(posologia).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PosologiaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Posologias
        [HttpPost]
        public async Task<IActionResult> PostPosologia([FromBody] Posologia posologia)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Posologia.Add(posologia);
            await _context.SaveChangesAsync();
            //DTO

            var dto = new PosologiaDTO
            {
                Id = posologia.Id,
                Descricao = posologia.Descricao
            };

            return CreatedAtAction("GetPosologia", new { id = posologia.Id }, dto);
        }

        // DELETE: api/Posologias/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePosologia([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var posologia = await _context.Posologia.SingleOrDefaultAsync(m => m.Id == id);
            if (posologia == null)
            {
                return NotFound();
            }

            _context.Posologia.Remove(posologia);
            await _context.SaveChangesAsync();

            return Ok(posologia);
        }

        private bool PosologiaExists(int id)
        {
            return _context.Posologia.Any(e => e.Id == id);
        }
    }
}