﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MedicamentoWebApi.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace MedicamentoWebApi.Models
{
    public class MedicamentoWebApiContext : IdentityDbContext<UserEntity>
    {
        public MedicamentoWebApiContext(DbContextOptions<MedicamentoWebApiContext> options) : base(options)
        {
        }

        public DbSet<MedicamentoWebApi.Models.Farmaco> Farmaco { get; set; }

        public DbSet<MedicamentoWebApi.Models.Apresentacao> Apresentacao { get; set; }

        public DbSet<MedicamentoWebApi.Models.Medicamento> Medicamento { get; set; }

        public DbSet<MedicamentoWebApi.Models.Posologia> Posologia { get; set; }
    }
}
