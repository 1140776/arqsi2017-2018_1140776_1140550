﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicamentoWebApi.DTOs
{
    public class ApresentacaoDTO
    {
        public int Id { get; set; }

        public string Forma { get; set; }

        public double Concentracao { get; set; }

        public double Quantidade { get; set; }
    }

    public class ApresentacaoParaReceitaDTO
    {
        public int Id { get; set; }

        public string Forma { get; set; }

        public double Concentracao { get; set; }

        public double Quantidade { get; set; }

        public string FarmacoNome{ get; set; }
   
    }
}
